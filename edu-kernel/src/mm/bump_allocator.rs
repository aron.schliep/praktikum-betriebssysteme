//! A very simple memory allocator.
//!
//! It actually isn't even allocating on the heap but in the bss section. Freed
//! Memory is never reused.

use crate::addr;
use crate::consts::CACHE_LINE;
use alloc::alloc::Layout;
use core::alloc::GlobalAlloc;
#[allow(unused_imports)]
use log::{debug, error, trace, warn};

/// Size of the preallocated space for the Bump Allocator.
pub const BUMP_MEM_SIZE: usize = 0x400000; // must be larger than the stacksize but smaller than the available memory

/// Alignment of pointers returned by the Bootstrap Allocator.
const ALIGNMENT: usize = CACHE_LINE;

/// This is a simple single-threaded implementation using some preallocated
/// space, along with an index variable. Freed memory is never reused.
pub struct BumpMemory {
	heap: [u8; BUMP_MEM_SIZE],
	index: usize,
}
impl BumpMemory {
	pub const fn new() -> BumpMemory {
		BumpMemory {
			heap: [0x0; BUMP_MEM_SIZE],
			index: 0,
		}
	}

	/// A very simple allocation using the always available Bootstrap Allocator.
	/// This is used to allocate memory for the datastructures of the real
	/// allocator
	pub fn alloc(&mut self, layout: Layout) -> *mut u8 {
		if self.index + layout.size() > BUMP_MEM_SIZE {
			error!("Bump allocator overflow! Increase BUMP_MEM_SIZE.");
			return core::ptr::null_mut();
		}

		let ptr = &mut self.heap[self.index] as *mut u8;
		debug!(
			"Allocating {:#X} bytes at {:#X}, index {}",
			layout.size(),
			ptr as usize,
			self.index
		);

		// Bump the heap index and align it up to the next ALIGNMENT boundary.
		self.index = addr::align_up(self.index + layout.size(), ALIGNMENT);

		ptr
	}

	pub fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		// We never deallocate memory of the Bootstrap Allocator.
		// It would only increase the management burden and we wouldn't save
		// any significant amounts of memory.
		trace!(
			"Deallocation in bump allocator of {} bytes at {:?}",
			layout.size(),
			ptr
		);
	}
}
impl Drop for BumpMemory {
	/// The Allocator potentially holds the memory for the real allocators. Thus
	/// it should never be deallocated.
	fn drop(&mut self) {
		panic!("Dropping bump allocator");
	}
}

static mut BUMP_MEM: BumpMemory = BumpMemory::new();
pub struct BumpMemoryAllocator;
unsafe impl GlobalAlloc for BumpMemoryAllocator {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		BUMP_MEM.alloc(layout)
	}

	unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		BUMP_MEM.dealloc(ptr, layout)
	}
}
