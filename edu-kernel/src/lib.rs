#![no_std] // Removes std from the prelude.

// Unstable feature gates
#![feature(abi_x86_interrupt)]
#![feature(alloc_error_handler)]
#![feature(asm)]
#![feature(const_fn)]
#![feature(const_fn_fn_ptr_basics)]
#![feature(const_panic)]
#![feature(int_bits_const)]
#![feature(naked_functions)]
#![feature(new_uninit)]
#![feature(option_unwrap_none)]
#![allow(incomplete_features)]
#![feature(specialization)]
// Test configuration
#![cfg_attr(test, no_main)] // #[no_main] attribute in test configuration
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]
// Lint configuration
#![deny(broken_intra_doc_links)]
#![allow(clippy::missing_safety_doc)]
#![deny(rust_2018_idioms)]

extern crate alloc;

#[cfg(test)]
#[macro_use]
extern crate edu_testmacro;

#[macro_use]
pub mod console;

pub mod addr;
pub mod arch;
pub mod consts;
pub mod logger;
pub mod mm;
pub mod scheduler;

mod once;

use console::Color;
use core::panic::PanicInfo;
use log::info;

/// Initializes the kernel systems. Must be called only once before the
/// application starts. This function is called from the architecture specific
/// boot code (e.g. in `arch/x86_64/kernel/start.rs`)
pub unsafe fn edu_kernel_init() {
	logger::init().unwrap();

	info!("Initializing kernel.");
	arch::init();

	info!("Arch specific initialization done");
	scheduler::init();
	info!("Scheduler initialization done");

	// enable interrupts => enable preemptive multitasking
	arch::interrupts::enable();
	//arch::system_timer::start();
	info!("Kernel initialization done");
}

/// This function is called on panic.
#[panic_handler]
pub fn panic(info: &PanicInfo<'_>) -> ! {
	arch::interrupts::disable();
	println!(
		"{reset}{red}[PANIC]{reset} {info}",
		reset = Color::Reset,
		red = Color::RedFG,
		info = info,
	);

	arch::processor::exit(1);
}

#[cfg(test)]
#[test]
fn kerneltest() {
	println!("-- Kernel Unit test");
}

/// Simple test framework. Runs all functions annotated with `[test-case]`
pub fn test_runner(tests: &[&dyn Fn()]) {
	println!();
	println!("======== edu-kernel Test Framework =========");
	println!("Running {} tests", tests.len());
	println!();
	for test in tests {
		test();
		println!();
	}
	println!("===== All tests successfully completed =====");
	println!();
}
