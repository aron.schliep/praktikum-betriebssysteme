//! Address manipulation.

/// Align address downwards.
///
/// Returns the greatest x with alignment `align` so that x <= addr. The
/// alignment must be  a power of 2.
///
/// Taken from [`x86_64::addr`].
#[inline]
pub const fn align_down(addr: usize, align: usize) -> usize {
	assert!(align.is_power_of_two(), "`align` must be a power of two");
	addr & !(align - 1)
}

/// Align address upwards.
///
/// Returns the smallest x with alignment `align` so that x >= addr. The
/// alignment must be a power of 2.
///
/// Taken from [`x86_64::addr`].
#[inline]
pub const fn align_up(addr: usize, align: usize) -> usize {
	assert!(align.is_power_of_two(), "`align` must be a power of two");
	let align_mask = align - 1;
	if addr & align_mask == 0 {
		addr // already aligned
	} else {
		(addr | align_mask) + 1
	}
}

#[inline]
pub const fn is_aligned(addr: usize, align: usize) -> bool {
	assert!(align.is_power_of_two(), "`align` must be a power of two");
	let align_mask = align - 1;
	addr & align_mask == 0
}
