//! Architecture specific CPU functionality.

use super::stack::BOOT_STACK;
use core::num::NonZeroU8;
use log::{debug, info};
use raw_cpuid::{CpuId, ExtendedFeatures, ExtendedFunctionInfo, FeatureInfo};
use x86_64::{
	addr::VirtAddr,
	instructions::port::Port,
	registers::{
		control::{Cr0, Cr0Flags, Cr4, Cr4Flags},
		model_specific::GsBase,
	},
};

/// Terminates the operating system with the specified exit code.
///
/// This function will never return and will immediately terminate the
/// operating system. The exit code is passed through to the host OS and will
/// be available for consumption by another process.
///
/// Note that no destructors on the current stack or in any other process
/// will be run. Make sure everything is terminated correctly before calling.
pub fn exit(code: u8) -> ! {
	info!("Shutting down.");
	unsafe {
		// ehyve's SHUTDOWN_PORT
		let mut exit_device = Port::new(0xf4);
		exit_device.write(code);
	}
	unreachable!()
}

/// Physical Address Bits.
///
/// [`Option`]`<`[`NonZeroU8`]`>` avoids false initializations and utilizes the
/// fact that zero is an invalid value for memory layout opmitizations.
static mut PHYSICAL_ADDRESS_BITS: Option<NonZeroU8> = None;

/// Physical Address Bits.
pub fn physical_address_bits() -> u8 {
	unsafe {
		PHYSICAL_ADDRESS_BITS
			.expect("CPU has not been initialized yet")
			.get()
	}
}

/// Whether the CPU supports 1GiB pages.
static mut SUPPORTS_1GIB_PAGES: Option<bool> = None;

/// Whether the CPU supports 1GiB pages.
pub fn supports_1gib_pages() -> bool {
	unsafe { SUPPORTS_1GIB_PAGES.expect("CPU has not been initialized yet") }
}

/// Configures the basic operation of the processor.
unsafe fn configure_cr0() {
	Cr0::update(|flags| {
		flags.insert(Cr0Flags::MONITOR_COPROCESSOR);
		flags.insert(Cr0Flags::NUMERIC_ERROR);
		flags.insert(Cr0Flags::ALIGNMENT_MASK);
		flags.remove(Cr0Flags::CACHE_DISABLE);
		debug!("CR0: {:?}", flags);
	})
}

/// Configures the protected mode (virtual memory, paging, multitasking).
unsafe fn configure_cr4(feature_info: Option<&FeatureInfo>, extended_features: &ExtendedFeatures) {
	Cr4::update(|flags| {
		assert!(
			extended_features.has_fsgsbase(),
			"CPU doesn't support FSGSBASE"
		);
		flags.insert(Cr4Flags::FSGSBASE);

		// Allow RDTSC or RDTSCP instructions in user space
		flags.remove(Cr4Flags::TIMESTAMP_DISABLE);

		flags.remove(Cr4Flags::PERFORMANCE_MONITOR_COUNTER);

		if feature_info.map_or(false, |info| info.has_pge()) {
			flags.insert(Cr4Flags::PAGE_GLOBAL);
		}

		if feature_info.map_or(false, |info| info.has_mce()) {
			flags.insert(Cr4Flags::MACHINE_CHECK_EXCEPTION);
		}

		debug!("CR4: {:?}", flags);
	})
}


/// Initializes [`PHYSICAL_ADDRESS_BITS`] and [`SUPPORTS_1GIB_PAGES`].
unsafe fn init_address_info(extended_function_info: &ExtendedFunctionInfo) {
	let physical_address_bits = extended_function_info
		.physical_address_bits()
		.expect("CPU doesn't support physical address bits");
	debug!("Physical address bits {}", physical_address_bits);
	PHYSICAL_ADDRESS_BITS = NonZeroU8::new(physical_address_bits);

	let has_1gib_pages = extended_function_info.has_1gib_pages();
	if has_1gib_pages {
		info!("CPU supports 1GiB pages");
	} else {
		debug!("CPU doesn't support 1GiB pages")
	}
	SUPPORTS_1GIB_PAGES = Some(has_1gib_pages);
}

/// Initializes the CPU.
pub unsafe fn init() {
	debug!("Configuring the processor");

	configure_cr0();

	let cpuid = CpuId::new();

	let extended_features = cpuid
		.get_extended_feature_info()
		.expect("CPU doesn't support extended feature information");

	configure_cr4(cpuid.get_feature_info().as_ref(), &extended_features);

	let extended_function_info = cpuid
		.get_extended_function_info()
		.expect("CPU doesn't support extended function information");

	// Setup GS segment
	GsBase::write(VirtAddr::new(BOOT_STACK.top() as u64));

	init_address_info(&extended_function_info);
}
