// Copyright (c) 2017 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! This module configures the 8284 programmable interval timer (PIT).

use super::time;
use crate::consts::TIMER_FREQ;
use core::sync::atomic::{AtomicBool, Ordering};
use log::debug;
use x86_64::instructions::port::Port;

static SYSTEM_TIMER_INIT: AtomicBool = AtomicBool::new(false);

/// The 8284 programmable interval timer (PIT).
///
/// This timer fires IRQ0 periodically and is used for preemption.
///
/// For an overview see Chapter 11, “8254 Timers,” in the Intel® 495 Series
/// Chipset Family On-Package Platform Controller Hub (PCH) Datasheet, Volume 1
/// of 2. For timer registers summary see Chapter 26, “8254 Timer,” in the
/// Intel® 495 Series Chipset Family On-Package Platform Controller Hub (PCH)
/// Datasheet, Volume 2 of 2
struct Timer8254();
impl Timer8254 {
	/// The 8254 unit's counter frequency in Hz.
	const COUNTER_FREQ: u32 = 1_193_181;

	/// Timer Control Word Register (TCW).
	const TCW_PORT: u16 = 0x43;

	/// Counter 0 - Counter Access Ports Register (C0_CAPR).
	const C0_CAPR: u16 = 0x40;

	/// Initialize the Timer.
	fn init() {
		debug!("Initializing 8254 Timer");
		// We don't need to do anything here. TODD: Drop this in the future
	}

	/// Start the system timer
	unsafe fn start() {
		debug!("STARTING TIMER");
		// Select a counter by writing a control word
		let mut tcw = Port::<u8>::new(Self::TCW_PORT);

		#[allow(clippy::unusual_byte_groupings)]
		{
			// 00 — Counter 0 select
			// 11 — Read/Write LSB then MSB
			// x10 — Rate generator (divide by n counter)
			// 0 — Binary countdown is used. The largest possible binary count is 2^16
			tcw.write(0b00_11_010_0);
		}

		// Wait some time for the timer to process
		time::native_io_delay();
		// Write an initial count for that counter
		let initial_count = (Self::COUNTER_FREQ / TIMER_FREQ) as u16;
		let [initial_count_lsb, initial_count_msb] = initial_count.to_le_bytes();
		let mut c0_capr = Port::<u8>::new(Self::C0_CAPR);
		c0_capr.write(initial_count_lsb);
		time::native_io_delay();
		c0_capr.write(initial_count_msb);
	}

	/// Stop the system timer
	unsafe fn stop() {
		debug!("STOPPING TIMER");
		


		let mut tcw = Port::<u8>::new(Self::TCW_PORT);

		#[allow(clippy::unusual_byte_groupings)]
		tcw.write(0b00110000);
		time::native_io_delay();

		let mut c0_capr = Port::<u8>::new(Self::C0_CAPR);
		c0_capr.write(0);
		time::native_io_delay();
		c0_capr.write(0);
		time::native_io_delay();

		
	}
}

/// Initialize the system timer
pub fn init() {
	SYSTEM_TIMER_INIT
		.compare_exchange(false, true, Ordering::SeqCst, Ordering::SeqCst)
		.expect("Timer8254 initialized twice");
	Timer8254::init()
}

/// Start the system timer
///
/// # Safety:
///
/// The timer must be initialized and have a correct handler. Otherwise this can
/// crash the system.
pub unsafe fn start() {
	if !SYSTEM_TIMER_INIT.load(Ordering::SeqCst) {
		panic!("Starting Timer8254 before initialization");
	}
	Timer8254::start()
}

/// Stop the system timer
///
/// # Safety:
///
/// This stops preemption and can thus lead to deadlocks.
pub unsafe fn stop() {
	if !SYSTEM_TIMER_INIT.load(Ordering::SeqCst) {
		panic!("Stopping system timer before initialization");
	}
	Timer8254::stop()
}
