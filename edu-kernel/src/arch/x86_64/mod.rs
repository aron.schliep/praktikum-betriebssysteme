// Copyright (c) 2017-2018 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

pub mod bootinfo;
pub mod cpu_context;
pub mod gdt;
pub mod interrupts;
pub mod processor;
pub mod serial;
pub mod stack;
mod start;
pub mod system_timer;
pub mod time;

use crate::consts::USER_ENTRY;

/// Initialize module, must be called once, and only once
pub unsafe fn init() {
	processor::init();
	gdt::init();
	interrupts::init();
	system_timer::init();
}

#[inline(never)]
#[naked]
pub fn jump_to_user_land(_f: extern "C" fn() -> !) -> ! {
	unsafe {
		asm!(
			// Calculate entry, put in rax
			"and edi, 0xFFF", // offset = _f & 0xFFF;
			"mov rax, {user_entry}",
			"or rax, rdi", // entry =  USER_ENTRY | offset

			"swapgs",
			"mov rbx, {ds}",
			"mov ds, rbx",
			"mov es, rbx",
			"push rbx",
			"mov rbx, {stack}",
			"push rbx",
			"pushfq",
			"mov rbx, {cs}",
			"push rbx",
			"push rax", // push entry
			"iretq",

			ds = const 0x23u64,
			cs = const 0x2bu64,
			user_entry = const USER_ENTRY,
			stack = const USER_ENTRY + 4 * 1024 * 1024,
			options(noreturn),
		)
	}
}
