//! Chapter 6, “Interrupt and Exception Handling,” in the Intel® 64 and IA-32
//! Architectures Software Developer’s Manual, Volume 3 (3A, 3B, 3C & 3D):
//! System Programming Guide

// Copyright (c) 2017-2018 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

pub use x86_64::instructions::interrupts::{
	are_enabled, disable, enable, without_interrupts as free,
};

use crate::scheduler;
use crate::singleton;
use super::time;
#[allow(unused_imports)]
use log::{debug, trace, warn};
use pic8259_simple::ChainedPics;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};

/// The interrupt offset of the first of the two [`PICS`].
const PIC_1_OFFSET: u8 = 32;

/// The interrupt offset of the second of the two [`PICS`].
const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[repr(u8)]
/// An interrupt index for external interrupts, triggered by [`PICS`] and
/// starting at [`PIC_1_OFFSET`].
enum InterruptIndex {
	Timer = PIC_1_OFFSET,
}

/// The 8259 Programmable Interrupt Controller (PIC).
///
/// The PIC manages hardware interrupts and fires the appropriate interrupt
/// handler.
///
/// In protected mode, the first 32 interrupt vectors are reserved and used for
/// CPU exceptions, which conflicts with the PICs default real mode behaviour.
/// This is why we have to adjust the PIC's offsets (remap the PIC) accordingly
/// ([`PIC_1_OFFSET`], [`PIC_2_OFFSET`]) to use non-reserved interrupt vectors.
static mut PICS: ChainedPics = unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) };

pub fn init() {
	//debug!("initialize interrupt descriptor table");

	let idt = singleton!(: InterruptDescriptorTable = InterruptDescriptorTable::new()).unwrap();
	idt.double_fault.set_handler_fn(double_fault_handler);
	idt[InterruptIndex::Timer as usize].set_handler_fn(timer_interrupt_handler);
	set_unhandler_fns(idt);

	debug!("idt at {:p}", idt);
	idt.load();

	unsafe { PICS.initialize() };
}

/// The double fault (`#DF`, [`InterruptDescriptorTable::double_fault`])
/// exception handler.
extern "x86-interrupt" fn double_fault_handler(
	stack_frame: &mut InterruptStackFrame,
	_error_code: u64,
) -> ! {
	panic!("Double fault\nstack_frame = {:#?}", stack_frame);
}

/// The timer interrupt handler triggered by the [`System
/// Timer`](super::system_timer).
extern "x86-interrupt" fn timer_interrupt_handler(stack_frame: &mut InterruptStackFrame) {
	debug!("INTERRUPT {}", time::timestamp());
	trace!("Timer Interrupt");
	trace!("stack_frame = {:#?}", stack_frame);
	unsafe { PICS.notify_end_of_interrupt(InterruptIndex::Timer as u8) }
	scheduler::get().yield_now();
	//debug!("RESCHEDULED");
}

/// Default interrupt handlers
///
/// This macro implements default interrupt handlers, panicking with debug
/// information. Not implementing these handlers would cause a double
/// fault and prevent us from finding the causing exception.
///
/// The implemented handlers have to be registered using `set_unhandler_fns`.
macro_rules! unhandled_interrupts {
	(
		HandlerFunc: {$(
			$handler_func:ident,
		)+},
		HandlerFuncWithErrCode: {$(
			$handler_func_with_err_code:ident,
		)+},
	) => {
		fn set_unhandler_fns(idt: &mut InterruptDescriptorTable) {
			$(
				idt.$handler_func.set_handler_fn($handler_func);
			)+
			$(
				idt.$handler_func_with_err_code.set_handler_fn($handler_func_with_err_code);
			)+
		}

		$(
			extern "x86-interrupt" fn $handler_func(stack_frame: &mut InterruptStackFrame) {
				unimplemented!(
					"Interrupt handler \"{}\"\nstack_frame = {:#?}",
					stringify!($handler_func),
					stack_frame
				)
			}
		)+
		$(
			extern "x86-interrupt" fn $handler_func_with_err_code(stack_frame: &mut InterruptStackFrame, error_code: u64) {
				unimplemented!(
					concat!(
						"Interrupt handler \"{}\"\n",
						"error_code = {}\n",
						"stack_frame = {:#?}"
					),
					stringify!($handler_func_with_err_code),
					error_code,
					stack_frame
				)
			}
		)+
	};
}

unhandled_interrupts!(
	HandlerFunc: {
		divide_error,
		debug,
		non_maskable_interrupt,
		breakpoint,
		overflow,
		bound_range_exceeded,
		invalid_opcode,
		device_not_available,
		x87_floating_point,
		simd_floating_point,
		virtualization,
	},
	HandlerFuncWithErrCode: {
		invalid_tss,
		segment_not_present,
		stack_segment_fault,
		general_protection_fault,
		alignment_check,
		security_exception,
	},
);
