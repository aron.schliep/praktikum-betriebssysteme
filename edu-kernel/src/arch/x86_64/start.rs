// Copyright (c) 2017-2020 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use crate::arch::stack::BOOT_STACK;
use crate::consts::STACK_SIZE;
use crate::edu_kernel_init;
#[cfg(not(test))]
use {crate::scheduler, core::ptr};

extern "C" {
	fn _init();
}

#[no_mangle]
#[naked]
pub extern "C" fn _start() -> ! {
	unsafe {
		asm!(
			// Initialize rsp with BOOT_STACK.top()
			"mov rsp, {boot_stack}",
			"add rsp, {top_offset}",

			// jump to start
			"call {start}",

			boot_stack = sym BOOT_STACK,
			top_offset = const STACK_SIZE - 16,
			start = sym start,
			options(noreturn)
		)
	}
}

extern "C" fn start() -> ! {
	unsafe { edu_kernel_init() }

	#[cfg(not(test))]
	{
		extern "C" fn init(_: *mut ()) {
			unsafe { _init() }
		}

		scheduler::get().spawn(init, ptr::null_mut());
		scheduler::get().exit(Ok(()))
	}

	#[cfg(test)]
	{
		crate::test_main();
		crate::arch::processor::exit(0)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	/// Verify correct boot stack offset
	fn test_boot_stack() {
		unsafe {
			assert_eq!(
				BOOT_STACK.top(),
				&BOOT_STACK as *const _ as usize + STACK_SIZE - 16,
			)
		}
	}
}
