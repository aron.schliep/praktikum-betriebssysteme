//! This module reexports the architecture specific Implementations of the
//! configured target at compile time, so that the general code can use commands
//! like `use arch::init` (which will then resolve to e.g.
//! `arch::x86_64::init`).


// Implementations for x86_64.
#[cfg(target_arch = "x86_64")]
pub mod x86_64;

// Export our platform-specific modules.
#[cfg(target_arch = "x86_64")]
pub use {
	self::x86_64::bootinfo::{get_memfile, get_memory_size},
	self::x86_64::cpu_context::CpuContext,
	self::x86_64::gdt::register_task,
	self::x86_64::serial::CONSOLE,
	self::x86_64::system_timer,
	self::x86_64::time::{delay, delay_yielding, timestamp},
	self::x86_64::{init, interrupts, processor, serial, stack},
};
