//! Task management

pub mod context;
pub mod fifo_scheduler;
pub mod priority_scheduler;
pub mod task;
pub mod thread;

use self::{fifo_scheduler::FifoScheduler, task::Priority};
use crate::arch::{self, interrupts, processor};
use crate::run_once;
use crate::scheduler::task::{Task, TaskStatus};
use alloc::boxed::Box;
use alloc::rc::Rc;
use core::{cell::RefCell, mem};
use log::{debug, error, info, warn};

static mut TASK_MANAGER: Option<TaskManager> = None;

/// Initialite module, must be called once, and only once
pub fn init() {
	custom_init(Box::new(FifoScheduler::new()));
}

/// Initialite module, but with a custom scheduler. Must be called once, and
/// only once, and [init] must not be called then.
pub fn custom_init<S: 'static + Scheduler>(scheduler: Box<S>) {
	// Ensure that a second initialization fails
	run_once!().unwrap();
	unsafe {
		TASK_MANAGER = Some(TaskManager::new(scheduler));
	}

	arch::register_task();
}

/// Returns a reference to this core's task manager.
pub fn get() -> &'static mut TaskManager {
	// SAFETY: The only critical section — schedule — is wrapped in yield_now.
	unsafe { TASK_MANAGER.as_mut().expect("Scheduling not initalized") }
}

/// A reference to a [`Task`].
///
/// This reference is single-threadedly reference-counted and dynamically
/// borrow-checked. [`Rc`] is fine, since there runs only one task manager per
/// core. Dynamic borrow checking is necessary to allow easily moving the boot
/// task and the current task around.
pub type TaskRef = Rc<RefCell<Task>>;

/// A scheduler.
///
/// Schedulers are used to manage all tasks which are ready to run.
///
/// This trait can be used to implement different scheduling algorithms.
/// Implementors can be directly plugged into [`TaskManager`].
pub trait Scheduler {
	/// Adds a new task to the scheduler.
	fn insert(&mut self, task: TaskRef);

	/// Returns the next task to execute.
	///
	/// Returns [`None`], if no tasks are available.
	fn next(&mut self) -> Option<TaskRef>;

	/// Wakes `task` up and adds it to the scheduler.
	fn wakeup(&mut self, task: TaskRef) {
		{
			let mut task_ref = task.borrow_mut();
			debug!("Waking up task {}", task_ref.id());
			assert_eq!(
				task_ref.status,
				TaskStatus::Blocked,
				"Can only wake up blocked tasks."
			);

			task_ref.status = TaskStatus::Ready;
		}
		self.insert(task)
	}
}

/// A single-core task manager.
///
/// This task manager is generic over a [`Scheduler`].
///
/// Not to be confused with one of the most popular Microsoft applications.
pub struct TaskManager {
	/// The current task.
	current_task: TaskRef,
	/// A finished task, ready for being freed.
	finished_task: Option<TaskRef>,
	/// The task scheduler.
	scheduler: Box<dyn Scheduler>,
}

impl TaskManager {
	/// Constructs a new task manager from the supplied scheduler.
	pub fn new<S: 'static + Scheduler>(scheduler: Box<S>) -> Self {
		let boot_task = Rc::new(RefCell::new(Task::new_boot()));

		TaskManager {
			current_task: boot_task,
			finished_task: None,
			scheduler,
		}
	}

	/// Spawns a new task.
	pub fn spawn(&mut self, f: extern "C" fn(*mut ()), arg: *mut ()) -> TaskRef {
		self.spawn_task(Task::new(f, arg))
	}

	/// Spawns a new task with the specified priority.
	pub fn spawn_prio(
		&mut self,
		f: extern "C" fn(*mut ()),
		arg: *mut (),
		prio: Priority,
	) -> TaskRef {
		self.spawn_task(Task::from_priority(f, arg, prio))
	}

	fn spawn_task(&mut self, task: Task) -> TaskRef {
		let task = Rc::new(RefCell::new(task));
		self.scheduler.insert(task.clone());
		task
	}

	/// Exits the current task.
	pub fn exit(&mut self, result: Result<(), ()>) -> ! {
		interrupts::free(|| {
			let id = self.current_task.borrow().id();
			match result {
				Ok(()) => info!("Finishing task {}", id),
				Err(()) => error!("Aborting task {}", id),
			}

			// TODO: destroy user space
			// drop_user_space();

			self.current_task.borrow_mut().status = TaskStatus::Finished;

			// Wakup joining tasks
			for task in self.current_task.borrow_mut().joining.drain(..) {
				self.scheduler.wakeup(task)
			}
		});
		self.yield_now();
		unreachable!()
	}

	/// Blocks the current task.
	///
	/// This sets the current task to [`TaskStatus::Blocked`] and returns the
	/// [`TaskRef`]. This function does not yield the current task. Instead you
	/// should store the returned task and prepare it for eventual wakeup. After
	/// storing the task away, call [`yield_now`](Self::yield_now) manually!
	pub fn block_current_task(&mut self) -> TaskRef {
		let mut task = self.current_task.borrow_mut();
		debug!("Blocking task {}", task.id());
		assert_eq!(
			task.status,
			TaskStatus::Running,
			"Can only block running tasks."
		);

		task.status = TaskStatus::Blocked;
		self.current_task.clone()
	}

	/// Wakes `task` up and adds it to the scheduler.
	pub fn wakeup_task(&mut self, task: TaskRef) {
		self.scheduler.wakeup(task)
	}

	/// Waits for `task` to finish.
	pub fn join(&mut self, task: TaskRef) {
		interrupts::free(|| {
			warn!("Joining {}", task.borrow().id());
			if task.borrow().status != TaskStatus::Finished {
				let blocked = self.block_current_task();
				task.borrow_mut().joining.push(blocked);
				self.yield_now();
			}
		})
	}

	/// Returns the start address of the stack
	#[no_mangle]
	pub fn get_current_stack(&self) -> usize {
		self.current_task.borrow().context().stack().bottom()
	}

	/// Yields the current task.
	pub fn yield_now(&mut self) {
		interrupts::free(|| self.schedule())
	}

	/// Schedules and may switch to another task.
	fn schedule(&mut self) {
		// Clean up any finished task.
		drop(self.finished_task.take());

		let current_status = self.current_task.borrow().status;
		match current_status {
			TaskStatus::Running => {
				self.scheduler.insert(self.current_task.clone());
				let new_task = self.scheduler.next().unwrap();
				if !Rc::ptr_eq(&self.current_task, &new_task) {
					// Switch to another task.
					self.current_task.borrow_mut().status = TaskStatus::Ready;
					self.switch(new_task)
				} else {
					// Continue with the current task.
				}
			}
			TaskStatus::Finished => {
				if let Some(new_task) = self.scheduler.next() {
					self.finished_task
						.replace(self.current_task.clone())
						.unwrap_none();
					self.switch(new_task)
				} else {
					// All tasks finished
					processor::exit(0)
				}
			}
			TaskStatus::Blocked => {
				// If the current task is blocked, there must be a ready task.
				let new_task = self.scheduler.next().unwrap();
				self.switch(new_task)
			}
			TaskStatus::Ready => unreachable!("Invalid state"),
		}
	}

	/// Switches to `task`.
	fn switch(&mut self, task: TaskRef) {
		debug!(
			"Switching from task {} to task {}.",
			self.current_task.borrow().id(),
			task.borrow().id()
		);

		let old_task = mem::replace(&mut self.current_task, task);
		self.current_task.borrow_mut().status = TaskStatus::Running;

		// Drop this task before switching out of it — we might never return.
		// self.finished task ensures, this is still valid and will clean up
		// eventually.
		let old_ptr = old_task.as_ptr();
		drop(old_task);

		// `RefCells` runtime borrow checker is cheated here. Otherwise, we would still
		// borrow although we switched to another context. This would lead to issues
		// when scheduling again.
		unsafe {
			old_ptr
				.as_mut()
				.unwrap()
				.switch(&mut self.current_task.as_ptr().as_mut().unwrap());
		}
	}
}

impl Default for TaskManager {
	fn default() -> Self {
		let sched = Box::new(FifoScheduler::new());
		Self::new(sched)
	}
}
