//! Tasks

use super::{context::Context, TaskRef};
use crate::consts::NR_PRIORITIES;
use alloc::vec::Vec;
use core::convert::TryFrom;
use core::fmt;
use core::num::NonZeroU64;
use core::sync::atomic::{AtomicU64, Ordering};

/// The status of the task - used for scheduling
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum TaskStatus {
	Ready,
	Running,
	Blocked,
	Finished,
}

/// A unique identifier for a task (i.e. `pid`).
#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy, Hash)]
pub struct TaskId(NonZeroU64);

impl TaskId {
	pub(crate) fn new() -> Self {
		static TASK_COUNT: AtomicU64 = AtomicU64::new(1);

		let id = TASK_COUNT.fetch_add(1, Ordering::Relaxed);

		Self(NonZeroU64::new(id).unwrap())
	}

	pub(crate) fn as_u64(&self) -> NonZeroU64 {
		self.0
	}
}

impl fmt::Display for TaskId {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		self.as_u64().fmt(f)
	}
}

/// The priority of a task.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Priority(u8);

impl Priority {
	pub const LOW: Self = unsafe { Self::new_unchecked(0) };
	pub const NORMAL: Self = unsafe { Self::new_unchecked(16) };
	pub const HIGH: Self = unsafe { Self::new_unchecked(24) };
	pub const REALTIME: Self = unsafe { Self::new_unchecked(NR_PRIORITIES as u8 - 1) };

	/// Creates a priority without checking the value.
	///
	/// # Safety
	///
	/// The value must be smaller than `NR_PRIORITIES`.
	pub const unsafe fn new_unchecked(n: u8) -> Self {
		Self(n)
	}

	/// Creates a priority if the given value is smaller than `NR_PRIORITIES`.
	pub const fn new(n: u8) -> Option<Self> {
		if n < NR_PRIORITIES {
			Some(Self(n))
		} else {
			None
		}
	}

	/// Returns the value as a primitive type.
	pub const fn get(self) -> u8 {
		self.0
	}
}

impl Default for Priority {
	fn default() -> Self {
		Self::NORMAL
	}
}

impl From<Priority> for u8 {
	fn from(priority: Priority) -> Self {
		priority.0
	}
}

impl TryFrom<u8> for Priority {
	type Error = &'static str;

	fn try_from(value: u8) -> Result<Self, Self::Error> {
		Self::new(value).ok_or("Priority only accepts value smaller than the number of priorities!")
	}
}

impl fmt::Display for Priority {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		self.get().fmt(f)
	}
}

/// A task control block, which identifies either a process or a thread
#[repr(align(64))]
#[derive(Debug)]
pub struct Task {
	/// The ID of this context
	id: TaskId,
	/// Task Priority
	prio: Priority,
	/// Status of a task, e.g. if the task is ready or blocked
	pub status: TaskStatus,
	/// The context of this task.
	context: Context,
	/// Tasks which are joining this task.
	pub joining: Vec<TaskRef>,
}

impl Task {
	/// Creates the task of the boot context.
	///
	/// This can only be run once! Further calls will panic.
	pub fn new_boot() -> Self {
		Self {
			id: TaskId::new(),
			prio: Default::default(),
			status: TaskStatus::Running,
			context: Context::boot(),
			joining: Vec::new(),
		}
	}

	/// Creates a new task running `f` with `arg`.
	pub fn new(f: extern "C" fn(*mut ()), arg: *mut ()) -> Self {
		Self::from_priority(f, arg, Default::default())
	}

	/// Creates a new task with the specified priority.
	pub fn from_priority(f: extern "C" fn(*mut ()), arg: *mut (), prio: Priority) -> Self {
		Self {
			id: TaskId::new(),
			prio,
			status: TaskStatus::Ready,
			context: Context::new(f, arg),
			joining: Vec::new(),
		}
	}

	/// Returns the task id of this task
	pub fn id(&self) -> TaskId {
		self.id
	}

	/// Returns the priority of this task.
	pub fn prio(&self) -> Priority {
		self.prio
	}

	/// Returns the context of this task.
	pub fn context(&self) -> &Context {
		&self.context
	}

	/// Performs a context switch from this task to `other`.
	pub fn switch(&mut self, other: &mut Self) {
		self.context.switch(&mut other.context)
	}
}
