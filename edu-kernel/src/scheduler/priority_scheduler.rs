//! Priority-based scheduling.

use crate::consts::NR_PRIORITIES;
use crate::scheduler::task::Priority;
use super::{Scheduler, TaskRef};
use alloc::collections::VecDeque;

/// A priority-based scheduler.
#[derive(Default)]
pub struct PriorityScheduler {
	queue: VecDeque<TaskRef>,
}

impl PriorityScheduler {
	pub fn new() -> Self {
		PriorityScheduler {
			queue: VecDeque::new(),
		}
	}
}

impl Scheduler for PriorityScheduler {
	fn insert(&mut self, task: TaskRef) {
		self.queue.push_back(task)
	}

	fn next(&mut self) -> Option<TaskRef> {
		let mut index = 0;
		let mut max = 0;
		for i in 0..self.queue.len(){
			if(self.queue[i].borrow().prio().get()>max){
				index = i;
				max = self.queue[i].borrow().prio().get();
			}
		}
		if index>0{
			self.queue.swap(index, 0);
		}
		self.queue.pop_front()		
	}
}
