//! Task context

use crate::{
	arch::{
		stack::{Stack, BOOT_STACK},
		CpuContext,
	},
	run_once,
};
use alloc::boxed::Box;
use core::ptr;
use log::trace;

/// A task context that can be switched to.
#[derive(Debug)]
pub struct Context {
	/// The stack of this context.
	///
	/// If this is [`None`], this is the context of the [`BOOT_STACK`].
	stack: Option<Box<Stack>>,
	/// The stack pointer for storing the CPU context.
	///
	/// On a context switch this is used to mark the position where a
	/// [`CpuContext`] was saved to and can be retrieved from again.
	cpu_context: *mut CpuContext,
}

impl Context {
	pub fn boot() -> Self {
		run_once!().unwrap();
		Self {
			stack: None,
			cpu_context: ptr::null_mut(),
		}
	}

	pub fn new(f: extern "C" fn(*mut ()), arg: *mut ()) -> Self {
		// Allocate an uninitialized stack on the heap.
		// Boxing an initialized stack fails.
		let mut stack = unsafe { Box::new_uninit().assume_init() };

		let last_stack_pointer = CpuContext::init(&mut stack, f, arg);
		Self {
			stack: Some(stack),
			cpu_context: last_stack_pointer,
		}
	}

	/// Returns the stack of the context.
	pub fn stack(&self) -> &Stack {
		self.stack
			.as_ref()
			.map(AsRef::as_ref)
			.unwrap_or(unsafe { &BOOT_STACK })
	}

	/// Switches to the supplied context.
	pub fn switch(&mut self, other: &mut Context) {
		trace!(
			"Switching context from {:p} to {:p}",
			self.cpu_context,
			other.cpu_context
		);
		CpuContext::switch(&mut self.cpu_context, other.cpu_context)
	}
}
