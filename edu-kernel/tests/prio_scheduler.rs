//! This test test the Prio scheduler.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[macro_use]
extern crate edu_kernel;

use alloc::{boxed::Box, vec, vec::Vec};
use edu_kernel::logger;
use log::debug;
use edu_kernel::scheduler::{self, priority_scheduler::PriorityScheduler, thread, task::Priority};

/// This vector acts as a log of the order of the exection of the threads
static mut THREADVEC: Vec<u8> = Vec::new();

// This is a cooperative task, that pushes it's id into the THREADVEC
fn push_vec(id: u8) {
	for _ in 0..3 {
		
		println!("Hello: {}", id);
		unsafe { THREADVEC.push(id) };

		// reschedule
		scheduler::get().yield_now();
	}
}

#[no_mangle]
/// A custom start function, as we want to modify the scheduler.
extern "C" fn _start() -> ! {
	println!("Prio Scheduler Test");

	// The Logger needs to be enabled for any kind of Kernel Message
	logger::init().unwrap();
	unsafe { edu_kernel::arch::init() };
	// ensure that we don't have timer interrupts
	//unsafe { edu_kernel::arch::system_timer::stop() };

	// initalize the scheduling with a Prio scheduler
	let sched = Box::new(PriorityScheduler::new());
	edu_kernel::scheduler::custom_init(sched);

	// Spawn three threads
	let threads = vec![	thread::spawn_prio(move || push_vec(0), Priority::new(1).unwrap()),
						thread::spawn_prio(move || push_vec(1), Priority::new(0).unwrap()),
						thread::spawn_prio(move || push_vec(2), Priority::new(1).unwrap())];

	// wait for all threads to finish
	for t in threads {
		t.join();
	}

	// TODO: Prüfen Sie, ob die Threads in der richtigen Reihenfolge ausgeführt wurden
	for i in 0..unsafe{THREADVEC.len()}{
		debug!("THREADVEC[{}] = {}",i, unsafe{THREADVEC[i]});
		//assert_eq!(i%3, unsafe{THREADVEC[i] as usize});
	}

	edu_kernel::arch::processor::exit(0)
}
