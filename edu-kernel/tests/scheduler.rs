//! This test spawns two task and runs them.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

use alloc::vec::Vec;
use core::{
	ptr,
	sync::atomic::{AtomicUsize, Ordering},
};
use edu_kernel::scheduler;

/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	static COUNT: AtomicUsize = AtomicUsize::new(0);

	// Each task atomically increments the variable COUNT
	extern "C" fn add(_: *mut ()) {
		COUNT.fetch_add(1, Ordering::Relaxed);
	}

	let tasks = (0..2)
		.map(|_| scheduler::get().spawn(add, ptr::null_mut()))
		.collect::<Vec<_>>();

	for task in tasks {
		scheduler::get().join(task);
	}

	assert_eq!(2, COUNT.load(Ordering::Relaxed))
}
