#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[macro_use]
extern crate edu_kernel;
use edu_kernel::arch::x86_64::time;
use edu_kernel::logger;
use log::debug;
use edu_kernel::scheduler::{self, fifo_scheduler::FifoScheduler, thread};
use alloc::{boxed::Box, vec, vec::Vec};


fn wait(){
    let time1 = time::timestamp();
    debug!("START {}", time1);
	while(time::timestamp()-time1<1000000000){
    }
    debug!("END {}", time::timestamp());
}

#[no_mangle]
/// A custom start function, as we want to modify the scheduler.
extern "C" fn _start() -> ! {
    logger::init().unwrap();
    unsafe { edu_kernel::arch::init() };
    //unsafe { edu_kernel::arch::system_timer::start() };
    unsafe { edu_kernel::arch::system_timer::stop() };
    let sched = Box::new(FifoScheduler::new());
	edu_kernel::scheduler::custom_init(sched);
	debug!("Timer TEST");


    
    let t = thread::spawn(move || wait());
    t.join();



	edu_kernel::arch::processor::exit(0)
}
