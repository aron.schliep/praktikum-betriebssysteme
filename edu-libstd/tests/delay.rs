//! This test spawns two task and runs them.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

use alloc::vec::Vec;
use edu_libstd::println;
use edu_libstd::thread::{self, Handle};
use edu_libstd::time;


const DELAY: u64 = 100_000_000;

fn test_cooperative_delay() -> u64 {
	let starttime = time::timestamp();

	let threads = (0..2)
		.map(|_| {
			thread::spawn(|| {
				time::cooperative_delay(DELAY);
				println!("finished")
			})
		})
		.collect::<Vec<_>>();

	threads.into_iter().for_each(Handle::join);

	time::timestamp() - starttime
}

fn test_busy_delay() -> u64 {
	let starttime = time::timestamp();

	let threads = (0..2)
		.map(|_| {
			thread::spawn(|| {
				time::busy_delay(DELAY);
				println!("finished")
			})
		})
		.collect::<Vec<_>>();

	threads.into_iter().for_each(Handle::join);

	time::timestamp() - starttime
}

/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	//unsafe { edu_kernel::arch::system_timer::start() };
	let cooperative_duration = test_cooperative_delay();
	let busy_duration = test_busy_delay();
	println!("cooperative duration {}", cooperative_duration);
	println!("busy duration        {}", busy_duration);

	assert!(cooperative_duration < busy_duration);
}
