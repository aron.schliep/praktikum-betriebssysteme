//! This test spawns two task and runs them.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

use alloc::sync::Arc;
use alloc::vec::Vec;
use core::sync::atomic::{AtomicUsize, Ordering};
use edu_libstd::thread::{self, Handle};

/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	let val = Arc::new(AtomicUsize::new(0));

	// create two threads which just increment val by one and then return that
	// value.
	let join_handles = (0..2)
		.map(|_| {
			let val = Arc::clone(&val);
			thread::spawn(move || val.fetch_add(1, Ordering::Relaxed))
		})
		.collect::<Vec<_>>();

	// Collect the return values of the threads
	let results = join_handles
		.into_iter()
		.map(Handle::join)
		.collect::<Vec<_>>();

	assert_eq!(2, val.load(Ordering::Relaxed));
	assert!(results.contains(&0));
	assert!(results.contains(&1));
}
