//! Thread functions from the kernel

pub use edu_kernel::scheduler::task::Priority;
use edu_kernel::scheduler::thread;
pub use edu_kernel::scheduler::thread::Handle;

/// Spawns a new thread.
pub fn spawn<T: Send + 'static>(f: impl FnOnce() -> T + Send + 'static) -> Handle<T> {
	thread::spawn(f)
}

pub fn spawn_prio<T: Send + 'static>(
	f: impl FnOnce() -> T + Send + 'static,
	prio: Priority,
) -> Handle<T> {
	thread::spawn_prio(f, prio)
}
