//! IO functions for the application. (`println!`)

pub use edu_kernel::arch::CONSOLE;

/// Prints to the console.
#[macro_export]
macro_rules! print {
	($($arg:tt)*) => ({
		use core::fmt::Write;
		let console = unsafe {&mut $crate::io::CONSOLE};
		write!(console, $($arg)*).unwrap();
	});
}

/// Prints to the console, with a newline.
#[macro_export]
macro_rules! println {
	($($arg:tt)*) => ({
		use core::fmt::Write;
		let console = unsafe {&mut $crate::io::CONSOLE};
		writeln!(console, $($arg)*).unwrap();
	});
}

#[cfg(test)]
mod tests {
	#[test]
	fn printtest() {
		print!("testprint");
		println!(" - testprintln");
	}
}
