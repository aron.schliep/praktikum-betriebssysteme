//! Busy and non-busy waiting in threads

use edu_kernel::arch::{delay, delay_yielding, timestamp as kernel_timestamp};

pub fn busy_delay(cycles: u64) {
	delay(cycles)
}

pub fn cooperative_delay(cycles: u64) {
	delay_yielding(cycles)
}

pub fn timestamp() -> u64 {
	kernel_timestamp()
}
