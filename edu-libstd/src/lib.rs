// Copyright (c) 2020 Stefan Lankes, RWTH Aachen University
//                    Jonathan Klimt, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

#![no_std] // Removes std from the prelude.

// Unstable feature gates
#![feature(asm)]
// Test configuration
#![cfg_attr(test, no_main)] // #[no_main] attribute in test configuration
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]
// Lint configuration
#![deny(broken_intra_doc_links)]
#![deny(rust_2018_idioms)]

#[cfg(test)]
#[macro_use]
extern crate edu_testmacro;

#[macro_use]
pub mod io;
pub mod memory;
pub mod thread;
pub mod time;

#[cfg(test)]
#[no_mangle]
/// This entry point is required to run unit tests of libstd
pub extern "C" fn _init() {
	test_main();
}

/// This function is the entry point, since the kernel starts a function
/// named `_main` in start.rs.
#[cfg(not(test))]
#[no_mangle]
pub extern "C" fn _init() {
	// Tell the compiler, that the function `main` is in a different crate and has
	// to be linked
	extern "Rust" {
		fn main();
	}
	// Start the application
	unsafe { main() }
}

/// Simple test framework. Runs all functions annotated with `[test-case]` or
/// `#[test]`
pub fn test_runner(tests: &[&dyn Fn()]) {
	println!();
	println!("======== edu-libstd Test Framework =========");
	println!("Running {} tests", tests.len());
	println!();
	for test in tests {
		test();
		println!();
	}
	println!("===== All tests successfully completed =====");
	println!();
}
