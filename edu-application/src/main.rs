// Don't link the standard library.
#![no_std]
// Disables emitting the main symbol. We implement it ourself.
#![no_main]
// Test configuration
#![feature(custom_test_frameworks)]
#![test_runner(edu_libstd::test_runner)]
#![reexport_test_harness_main = "test_main"]
#![deny(broken_intra_doc_links)]
#![deny(rust_2018_idioms)]

#[macro_use]
extern crate edu_libstd;

#[cfg(test)]
#[macro_use]
extern crate edu_testmacro;

/// This function is the entry point, since the edu-libstd starts a function
/// named `main` by default. It is important not to mangle the name of this
/// function. Otherwise the linker does not find it
#[no_mangle]
#[cfg(not(test))]
fn main() {
	println!("Hello from eduOS-rs!");
}

#[no_mangle]
#[cfg(test)]
/// When we run cargo test, we don't want to start the normal main but our test
/// framework instead
fn main() {
	test_main();
}

#[cfg(test)]
mod tests {
	#[test]
	fn run_println() {
		println!("Hello from: {}:{}", file!(), line!())
	}
}
